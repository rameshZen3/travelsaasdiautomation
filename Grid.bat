@echo off

start /d "V:\CwtDigitalAutomation\Driverchrome\chromedriver.exe" java -jar selenium-server-standalone-2.42.2.jar -role hub

start /d "V:\CwtDigitalAutomation\Driverchrome\chromedriver.exe" java -jar selenium-server-standalone-2.42.2.jar -role webdriver -hub  http://localhost:4444/grid/register -port 5556 -browser browserName=firefox,maxInstances=5

start /d "V:\CwtDigitalAutomation\Driverchrome\chromedriver.exe" java -Dwebdriver.ie.driver="V:\CwtDigitalAutomation\Driverchrome\IEDriverServer.exe" -jar selenium-server-standalone-2.42.2.jar -role webdriver -hub  http://localhost:4444/grid/register -port 5557 -browser "browserName=internet explorer,version=11,platform=WINDOWS,maxInstances=5"

start /d "V:\CwtDigitalAutomation\Driverchrome\chromedriver.exe" java -Dwebdriver.chrome.driver="V:\CwtDigitalAutomation\Driverchrome\chromedriver.exe" -jar selenium-server-standalone-2.42.2.jar -role webdriver -hub  http://localhost:4444/grid/register -port 5558 -browser browserName=chrome,maxInstances=5

pause
