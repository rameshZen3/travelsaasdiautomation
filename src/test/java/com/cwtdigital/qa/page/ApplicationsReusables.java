package com.cwtdigital.qa.page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SearchTerm;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;

import com.cwtdigital.qa.base.BasePage;
import com.cwtdigital.qa.util.CommonMethod;
import com.thoughtworks.selenium.SeleniumException;

import junit.framework.AssertionFailedError;

public class ApplicationsReusables extends BasePage {
	
	public static Logger APPLICATION_LOGS = LoggerFactory
			.getLogger(ApplicationsReusables.class.getName());
	ITestResult result;
	String gmailBodyText = null;
	
	/**
	 * Capturing ScreenShot on success test cases
	 * @param driver
	 * @param methodName
	 */
	public void captureScreenshotOnSuccess(WebDriver driver, String methodName){
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(System.
					getProperty("user.dir")+"\\TestJarFunc\\screenshotsOnSuccess"+"\\"+"screenshotOnSuccess"+"_"+methodName+common.getCurrentTimeInstance()+"."+"png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * getPasswordFromGmail
	 * @param userEmail
	 * @param userEmailPassword
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public String getPasswordFromGmail(String userEmail,
			String userEmailPassword) throws InterruptedException, IOException {

		Properties props = new Properties();
		props.setProperty("mail.store.protocol", "imaps");
		File tempFile;

		try {
			Session session = Session.getInstance(props, null);
			System.out.println("########## session is #####" + session);
			Store store = session.getStore();
			System.out.println(userEmail + "," + userEmailPassword);
			store.connect("imap.gmail.com", userEmail, userEmailPassword);
			Thread.sleep(5000);
			Folder inbox = store.getFolder("INBOX");
			Thread.sleep(2000);
			inbox.open(Folder.READ_ONLY);
			Thread.sleep(2000);
			Message msg = inbox.getMessage(1);

			SearchTerm searchTerm = new SearchTerm() {

				@Override
				public boolean match(Message message) {
					try {
						if (message
								.getSubject()
								.contains(
										"Passenger Details")) {
							return true;
						}
					} catch (MessagingException ex) {
						ex.printStackTrace();
					}
					return false;
				}
			};
			Message[] foundMessages = inbox.search(searchTerm);
			List<Message> totalMessageList = Arrays.asList(foundMessages);
			Collections.sort(totalMessageList, new Comparator<Message>() {
				public int compare(Message o1, Message o2) {
					try {
						return o1.getReceivedDate().compareTo(
								o2.getReceivedDate());
					} catch (MessagingException e) {
						e.printStackTrace();
					}
					return 0;
				}
			});
			msg = totalMessageList.get(totalMessageList.size() - 1);

			Object content = msg.getContent();
			if (content instanceof Multipart) {
				Multipart mp = (Multipart) content;
				BodyPart bp = mp.getBodyPart(0);

				System.out.println("SENT DATE:" + msg.getSentDate());
				System.out.println("SUBJECT:" + msg.getSubject());
				System.out.println("CONTENT:" + bp.getContent());
				gmailBodyText = (String) bp.getContent();
			} else {
				gmailBodyText = content.toString();
			}
			Pattern pattern = Pattern
					.compile("(?si)http(.*)");
			Matcher m = pattern.matcher(gmailBodyText);
			if (m.find()) {
				gmailBodyText = m.group(1).trim();
			}
			System.out.println("msgBody CONTENT:" + gmailBodyText);

		} catch (Exception mex) {
			mex.printStackTrace();
		}
		System.out.println("returned gmailBodyText inside method>>> : "
				+ gmailBodyText);
		return gmailBodyText;

	}

	/**
	 * Method to authenticate OW
	 * @param driver
	 * @param authPassword
	 */
	public void authenticateToLoingOW(WebDriver driver, String authPassword) {
		
		try{
			
			//Send the password to the input field
			common.isElementPresent(driver, "auth_page_password_inputBox");
			common.sendKeys(driver, "auth_page_password_inputBox", authPassword);
			
			//Click on login button
			common.isElementPresent(driver, "auth_page_login_btn");
			common.clickByJavaScript(driver, "auth_page_login_btn");
			
		}catch(SeleniumException se) {
			se.printStackTrace();
		}
	}
	
	/**
	 * Method to verify all the traveler options page wrappers
	 * @param driver
	 */
	public void verifyTravellerOptionsPageWrappers(WebDriver driver) {
		
		try{
			common.isElementPresent(driver, "trav_optionsPage_traveller_options_sectionWrapper");
			common.isElementPresent(driver, "trav_optionsPage_login_sectionWrappper");
			common.isElementPresent(driver, "trav_optionsPage_itinerary_sectionWrapper");
			
		}catch(SeleniumException se) {
			se.printStackTrace();
		}
	}
	/**
	 * Method to create a new itinerary
	 * @param driver
	 * @throws InterruptedException 
	 */
	public void cretaeNewItinerary(WebDriver driver,Hashtable<String,String> data) throws InterruptedException{
		
		try{
			if(common.isElementPresent(driver,"start_new")){
				common.click(driver,"start_new");
				Thread.sleep(2000);
				common.click(driver, "trav_options_newItinerary");
				Thread.sleep(2000);
				/*common.selectDropdown(driver,"select_adult", data.get("Adult"));
				Thread.sleep(2000);
				common.selectDropdown(driver,"select_child", data.get("Child"));
				Thread.sleep(2000);*/
				common.click(driver, "start_new_itinerary");
				Thread.sleep(2000);
				common.sendKeys(driver, "select_cities", data.get("Cities"));
				common.click(driver, "submit_cities");
				Thread.sleep(2000);
				
			}
			else{
				/*common.selectDropdown(driver,"select_adult", data.get("Adult"));
				Thread.sleep(2000);
				common.selectDropdown(driver,"select_child", data.get("Child"));
				Thread.sleep(2000);*/
				common.click(driver, "trav_options_newItinerary");
				Thread.sleep(2000);
				common.click(driver, "start_new_itinerary");
				Thread.sleep(2000);
				common.sendKeys(driver, "select_cities", data.get("Cities"));
				common.click(driver, "submit_cities");
				Thread.sleep(2000);
				
			}
			
			
		}catch(SeleniumException se) {
			se.printStackTrace();
		}
	}
	
	/**
	 * Method to build the itinerary
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	public void buildItinerary(WebDriver driver) throws InterruptedException{
		try{
			common.isElementPresent(driver, "selectcities_proceed_booking");
			common.click(driver, "selectcities_proceed_booking");
		} catch(SeleniumException se){
			se.printStackTrace();
		}
	}
	
	public void preferedAirline(WebDriver driver,Hashtable<String,String> data) throws InterruptedException{
		try{
			common.isElementPresent(driver, "choose_flight_airline");
			common.selectDropdown(driver, "choose_flight_airline", data.get("Airline"));
			Thread.sleep(1000);
		} catch(SeleniumException se){
			se.printStackTrace();
		}
	}
	
	/**
	 * Method to select the date for segment1
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	public void selectDate1(WebDriver driver) throws InterruptedException{
		try{
		common.click(driver,"choose_flightspage_Select_date1");
		Thread.sleep(2000);
		}catch(SeleniumException se){
			se.printStackTrace();
			}
	}
	
	public void selectionDate1(WebDriver driver,Hashtable<String,String> data) throws InterruptedException
		{
			try{
				List<WebElement> elements=driver.findElements(By.xpath(".//*[@id='calendar']/div/div/div/div[@class='ui-datepicker-title']"));
				for(int i=0;i<elements.size();i++)
				{
					System.out.println(elements.get(i).getText());
					//selecting month			
					if(elements.get(i).getText().equals(data.get("Month_year1")))
					{
						//selecting date
						List <WebElement> days=driver.findElements(By.xpath(".//*[@id='calendar']/div[1]/div["+(i+1)+"]/table/tbody/tr/td"));
						for(WebElement d:days){
							System.out.println(d.getText());
							if(d.getText().equals(data.get("Day1"))){
								d.click();
								common.click(driver,"select_date_submit");
								Thread.sleep(2000);
								return;
							}
						}
					}
				}
				common.click(driver, "select_month_next");
				common.click(driver, "select_month_next");
				selectionDate1(driver,data);
				  
			    }catch(SeleniumException se){
				se.printStackTrace();
				}
		}
	
	
	/**
	 * Method to select the date for segment2
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	
	public void selectDate2(WebDriver driver) throws InterruptedException{
		try{
		common.click(driver,"choose_flightspage_Select_date2");
		Thread.sleep(2000);
		}catch(SeleniumException se){
			se.printStackTrace();
			}
	}
	
	public void selectionDate2(WebDriver driver,Hashtable<String,String> data) throws InterruptedException
	{
		//common.click(driver,"choose_flightspage_Select_date2");
		//Thread.sleep(3000);
		try{
			
			List<WebElement> elements=driver.findElements(By.xpath(".//*[@id='calendar']/div/div/div/div[@class='ui-datepicker-title']"));
			for(int i=0;i<elements.size();i++)
			{
				System.out.println(elements.get(i).getText());
				//selecting month			
				if(elements.get(i).getText().equals(data.get("Month_year2")))
				{
					//selecting date
					//List <WebElement> days=driver.findElements(By.xpath(".//*[@id='calendar']/div/div/table/tbody/tr/td/a"));
					List <WebElement> days=driver.findElements(By.xpath(".//*[@id='calendar']/div/div["+(i+1)+"]/table/tbody/tr/td"));
					for(WebElement d:days){
						System.out.println(d.getText());
						if(d.getText().equals(data.get("Day2"))){
							d.click();
							common.click(driver,"select_date_submit");
							Thread.sleep(2000);
							return;
						}
					}
				}
					
				}
			common.click(driver, "select_month_next");
			common.click(driver, "select_month_next");
			selectionDate2(driver,data);


			
		}catch(SeleniumException se){
			se.printStackTrace();

			
	}
	}
	
	/**
	 * Method to select the date for segment3
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	
	public void selectDate3(WebDriver driver) throws InterruptedException{
		try{
		common.click(driver,"choose_flightspage_Select_date3");
		Thread.sleep(2000);
		}catch(SeleniumException se){
			se.printStackTrace();
			}
	}
	

	public void selectionDate3(WebDriver driver,Hashtable<String,String> data) throws InterruptedException
	{
		try{
			
			List<WebElement> elements=driver.findElements(By.xpath(".//*[@id='calendar']/div/div/div/div[@class='ui-datepicker-title']"));
			for(int i=0;i<elements.size();i++)
			{
				System.out.println(elements.get(i).getText());
				//selecting month			
				if(elements.get(i).getText().equals(data.get("Month_year3")))
				{
					//selecting date
					List <WebElement> days=driver.findElements(By.xpath(".//*[@id='calendar']/div[1]/div["+(i+1)+"]/table/tbody/tr/td"));
					for(WebElement d:days){
						System.out.println(d.getText());
						if(d.getText().equals(data.get("Day3"))){
							d.click();
							common.click(driver,"select_date_submit");
							Thread.sleep(2000);
							return;
						}
					}
				}
					
				}
			common.click(driver, "select_month_next");
			Thread.sleep(2000);
			selectionDate3(driver,data);
			
		    }catch(SeleniumException se){
			se.printStackTrace();
	        }
	}
	
	/**
	 * Method to select the date for segment4
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	public void selectDate4(WebDriver driver) throws InterruptedException{
		try{
		common.click(driver,"choose_flightspage_Select_date4");
		Thread.sleep(2000);
		}catch(SeleniumException se){
			se.printStackTrace();
			}
	}
	
	public void selectionDate4(WebDriver driver,Hashtable<String,String> data) throws InterruptedException
	{
		//common.click(driver,"choose_flightspage_Select_date4");
		//Thread.sleep(3000);
		try{
			
			List<WebElement> elements=driver.findElements(By.xpath(".//*[@id='calendar']/div/div/div/div[@class='ui-datepicker-title']"));
			for(int i=0;i<elements.size();i++)
			{
				System.out.println(elements.get(i).getText());
				//selecting month			
				if(elements.get(i).getText().equals(data.get("Month_year4")))
				{
					//selecting date
					List <WebElement> days=driver.findElements(By.xpath(".//*[@id='calendar']/div[1]/div["+(i+1)+"]/table/tbody/tr/td"));
					for(WebElement d:days){
						System.out.println(d.getText());
						if(d.getText().equals(data.get("Day4"))){
							d.click();
							common.click(driver,"select_date_submit");
							Thread.sleep(3000);
							return;
						}
					}
				}
					
				}
			common.click(driver, "select_month_next");
			common.click(driver, "select_month_next");
			Thread.sleep(2000);
			selectionDate4(driver,data);

		}catch(SeleniumException se){
			se.printStackTrace();
		}
	}
	
	/**
	 * Method to select the date for segment5
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	
	public void selectDate5(WebDriver driver) throws InterruptedException{
		try{
		common.click(driver,"choose_flightspage_Select_date5");
		Thread.sleep(2000);
		}catch(SeleniumException se){
			se.printStackTrace();
			}
	}
	
	public void selectionDate5(WebDriver driver,Hashtable<String,String> data) throws InterruptedException
	{
		try{
			
			List<WebElement> elements=driver.findElements(By.xpath(".//*[@id='calendar']/div/div/div/div[@class='ui-datepicker-title']"));
			for(int i=0;i<elements.size();i++)
			{
				System.out.println(elements.get(i).getText());
				//selecting month			
				if(elements.get(i).getText().equals(data.get("Month_year5")))
				{
					//selecting date
					List <WebElement> days=driver.findElements(By.xpath(".//*[@id='calendar']/div[1]/div["+(i+1)+"]/table/tbody/tr/td"));
					for(WebElement d:days){
						System.out.println(d.getText());
						if(d.getText().equals(data.get("Day5"))){
							d.click();
							common.click(driver,"select_date_submit");
							Thread.sleep(3000);
							return;
						}
					}
				}
					
			}
			common.click(driver, "select_month_next");
			common.click(driver, "select_month_next");
			Thread.sleep(3000);
			selectionDate5(driver,data);
			
		}catch(SeleniumException se){
			se.printStackTrace();		
	        }
	}
	
	/**
	 * Method to select the date for segment6
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	public void selectDate6(WebDriver driver) throws InterruptedException{
		try{
		common.click(driver,"choose_flightspage_Select_date6");
		Thread.sleep(2000);
		}catch(SeleniumException se){
			se.printStackTrace();
			}
	}
	
	public void selectionDate6(WebDriver driver,Hashtable<String,String> data) throws InterruptedException
	{
			try{
			
			List<WebElement> elements=driver.findElements(By.xpath(".//*[@id='calendar']/div/div/div/div[@class='ui-datepicker-title']"));
			for(int i=0;i<elements.size();i++)
			{
				System.out.println(elements.get(i).getText());
				//selecting month			
				if(elements.get(i).getText().equals(data.get("Month_year6")))
				{
					//selecting date
					List <WebElement> days=driver.findElements(By.xpath(".//*[@id='calendar']/div[1]/div["+(i+1)+"]/table/tbody/tr/td"));
					for(WebElement d:days){
						System.out.println(d.getText());
						if(d.getText().equals(data.get("Day6"))){
							d.click();
							common.click(driver,"select_date_submit");
							Thread.sleep(3000);
							return;
						}
					}
				}
					
			}
			common.click(driver, "select_month_next");
			common.click(driver, "select_month_next");
			Thread.sleep(3000);
			selectionDate5(driver,data);
			
		}catch(SeleniumException se){
			se.printStackTrace();		
	        }
	}
	
	/**
	 * Method to select the choose flights and price
	 * @param driver
	 * @throws InterruptedException 
	 */

	public void pricemyItinerary(WebDriver driver) throws InterruptedException{
		try{
			common.click(driver,"choose_flights_price_myitinerary");
			Thread.sleep(40000);
		   }catch(SeleniumException se){
			se.printStackTrace();
		    }
          }

	/**
	 * Method to proceed booking
	 * @param driver
	 * @throws InterruptedException 
	 */
	public void proceedBooking(WebDriver driver) throws InterruptedException{
		try{
			common.isElementPresent(driver,"choose_flights_proceed_booking");
			common.click(driver,"choose_flights_proceed_booking");
			Thread.sleep(3000);
		   }catch(SeleniumException se){
			se.printStackTrace();
		    }
          }
		
	/**
	 * Method to specify traveler details
	 * @param driver
	 * @param data
	 * @throws InterruptedException 
	 */
	public void specifyTravellerDetails(WebDriver driver,Hashtable<String,String> data) throws InterruptedException{
		try{
			common.click(driver,"review_traveller_details");
			Thread.sleep(3000);
			common.selectDropdown(driver, "specify_travelers_title", data.get("Title"));
			common.sendKeys(driver, "specify_travelers_lastname", data.get("Lastname"));
			common.sendKeys(driver, "specify_travelers_firstname", data.get("Firstname"));
			common.sendKeys(driver, "specify_travelers_email", data.get("Email"));
			common.sendKeys(driver, "specify_travelers_confirmemail", data.get("Confirm Email"));
			common.sendKeys(driver, "specify_travelers_phone", data.get("Phone"));
		    common.click(driver,"remember_traveler_details");
			Thread.sleep(2000);
			common.click(driver, "specify_travelers_purchase");
			Thread.sleep(3000);
			common.selectDropdown(driver, "specify_travelers_gender", data.get("Gender"));
			common.selectDropdown(driver, "specify_travelers_date", data.get("DD"));
			common.selectDropdown(driver, "specify_travelers_month", data.get("MM"));
			common.selectDropdown(driver, "specify_travelers_year", data.get("YYYY"));
			common.click(driver,"specify_travelers_continue");
			Thread.sleep(3000);
					
		   }catch(SeleniumException se){
			se.printStackTrace();
		    }
          }
	

	/**
	 * Method to purchase trip
	 * @param driver
	 * @throws InterruptedException 
	 */
	public void purchaseTRip(WebDriver driver,Hashtable<String,String> data) throws InterruptedException{
		try{
			if(common.isElementPresent(driver,"purchase_card_type"))
			{
			common.selectDropdown(driver, "purchase_card_type", data.get("Cardtype"));
			common.sendKeys(driver, "purchase_vi_number1", data.get("Cardnumber1"));
			common.sendKeys(driver, "purchase_vi_number2", data.get("Cardnumber2"));
			common.sendKeys(driver, "purchase_vi_number3", data.get("Cardnumber3"));
			common.sendKeys(driver, "purchase_vi_number4", data.get("Cardnumber4"));
			common.sendKeys(driver,"purchase_ver_number", data.get("Ver_number"));
			common.selectDropdown(driver,"purchase_card_month", data.get("Exp_mon"));
			common.selectDropdown(driver, "purchase_card_year", data.get("Exp_year"));
			common.sendKeys(driver, "purchase_cardholder_name", data.get("Cardholder_name"));
			common.sendKeys(driver,"purchase_address", data.get("Address1"));
			common.sendKeys(driver, "purchase_city", data.get("City"));
			common.sendKeys(driver, "purchase_country", data.get("Country"));
			common.click(driver, "purchase_cardtype_agree_terms");	
			common.click(driver, "purchase_cardtype_agree_conditions");
			common.click(driver, "purchase_cardtype_click_purchase");	
			Thread.sleep(15000);
		     }
			else{
				common.click(driver, "purchase_agree_terms");	
				common.click(driver, "purchase_agree_conditions");
				common.click(driver, "purchase_confirm_trip");	
				Thread.sleep(15000);				
			}
											
		   }catch(SeleniumException se){
			 se.printStackTrace();
		     }
	} 
	
		
	/**
	 * Method to Save Itinerary
	 * @param driver
	 * @throws InterruptedException 
	 */
	public void saveItinerary(WebDriver driver,Hashtable<String,String> data) throws InterruptedException{
		try{
			common.click(driver, "save_itinerary");
			Thread.sleep(2000);
			if(common.isElementPresent(driver, "login_email_address"))
			  {
				common.sendKeys(driver,"login_email_address",conf.getValue("username"));
				common.sendKeys(driver,"login_password",conf.getValue("password"));
				common.click(driver, "login");
				Thread.sleep(2000);
				common.sendKeys(driver,"save_itinerary_name",data.get("Itinerary name"));
				Thread.sleep(2000);
				common.click(driver, "save_itinerary_save");
				common.click(driver, "save_itinerary_ok");
				Thread.sleep(2000);
			  }
			else{
				common.sendKeys(driver,"save_itinerary_name",data.get("Itinerary name"));
				Thread.sleep(2000);
				common.click(driver, "save_itinerary_save");
				common.click(driver, "save_itinerary_ok");
				Thread.sleep(2000);			
			    }
		    }catch(SeleniumException se){
			se.printStackTrace();
		    }
		} 
	
	/**
	 * Method to My Itineraries
	 * @param driver
	 * @throws InterruptedException 
	 */	
	public void myItineraries(WebDriver driver) throws InterruptedException{
		try{
			common.click(driver, "my_itineraries");
			Thread.sleep(2000);
			if(common.isElementPresent(driver, "login_email_address"))
			  {
				common.sendKeys(driver,"login_email_address",conf.getValue("username"));
				common.sendKeys(driver,"login_password",conf.getValue("password"));
				common.click(driver, "login");
				Thread.sleep(2000);
			    common.click(driver, "my_itineraries_open");
			    Thread.sleep(2000);
			  }
			else{
				 common.click(driver, "my_itineraries_open");
				 Thread.sleep(2000);				
			}
				
		  }catch(SeleniumException se){
			se.printStackTrace();
		    }
		} 

}
// End of Class