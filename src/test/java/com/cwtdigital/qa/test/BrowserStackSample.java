package com.cwtdigital.qa.test;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class BrowserStackSample {
	
	String username = "cwtbrowserstack1";
	String AUTOMATE_KEY = "zx2EhYyja2eQyynnnyxp";
	String URL = "http://" + username + ":" + AUTOMATE_KEY + "@hub.browserstack.com/wd/hub";
	
	//Browserstack URL to get the Capabilities (https://www.browserstack.com/automate/java)
	
	@Test
	public void test() throws MalformedURLException, InterruptedException{
		
		DesiredCapabilities caps = new DesiredCapabilities();
			
		//Capabilities for Firefox
		/*caps.setCapability("browser", "Firefox");
		caps.setCapability("browser_version", "46.0");
		caps.setCapability("os", "Windows");
		caps.setCapability("os_version", "10");
		caps.setCapability("resolution", "1024x768");
		caps.setCapability("browserstack.debug", "true");*/
	    

		caps.setCapability("browserName", "iPhone");
		caps.setPlatform(Platform.MAC);
		caps.setCapability("device", "iPhone 5");
		/*//Capabilities for IE
		caps.setCapability("browser", "IE");
		caps.setCapability("browser_version", "11.0");
		caps.setCapability("os", "Windows");
		caps.setCapability("os_version", "10");
		caps.setCapability("resolution", "1024x768");
		caps.setCapability("deviceOrientation", "portrait");*/
		
		/*//Capabilities for Android OS
		caps.setCapability("browserName", "android");
		caps.setCapability("platform", "ANDROID");
		caps.setCapability("device", "Samsung Galaxy S5");
		caps.setCapability("deviceOrientation", "portrait");*/
		
		/*//Capabilities for IOS
		caps.setCapability("browserName", "iPhone");
		caps.setCapability("platform", "MAC");
		caps.setCapability("device", "iPhone 6 Plus");
	    caps.setCapability("browserstack.debug", "true");
	    caps.setCapability("deviceOrientation", "portrait");*/
	    
	    //Scenario-1
	    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
	    driver.get("http://www.google.com");
        System.out.println("Page title is: " + driver.getTitle());
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("BrowserStack");
        element.submit();
        System.out.println("And the Title after submission is: " + driver.getTitle());
	    driver.quit();
	    
	   /* //Scenario-2
	    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
	    driver.get("http://www.easyjet.com/en/holidays/");
        System.out.println("Page title is: " + driver.getTitle());
        Thread.sleep(5000);
        List<WebElement> destinationList = driver.findElements(By.xpath("//select[@id='find_destinations']/option"));
        System.out.println("----------------------");
        for (int i = 1; i <= destinationList.size(); i++) {
        	System.out.println("Destination number " + i + " : " + destinationList.get(i).getText());
        	Thread.sleep(1000);
		}
        System.out.println("+++++++++++++++++++++++++++++++");
        driver.quit();*/
	}

}
