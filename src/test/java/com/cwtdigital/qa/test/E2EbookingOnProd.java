package com.cwtdigital.qa.test;

import java.util.Hashtable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.cwtdigital.qa.base.TestBase;
import com.cwtdigital.qa.page.ApplicationsReusables;
import com.cwtdigital.qa.util.CommonMethod;
import com.cwtdigital.qa.util.TestUtil;

public class E2EbookingOnProd extends TestBase {

	public static Logger APPLICATION_LOGS = LoggerFactory.getLogger(E2EbookingOnProd.class.getName());
	
	
	TestUtil util = new TestUtil();
	CommonMethod common = new CommonMethod(System.getProperty("user.dir") + "/locator/objectRepository.properties");
	ApplicationsReusables apage = new ApplicationsReusables();
	
		@DataProvider
	public Object[][] getDataFor_1() {
		return TestUtil.getData(apage.admin_xls, "OneWorldDataSheet", "testVerifyEndToEndBooking_01");
	}

	@SuppressWarnings("static-access")
	@Test(enabled = true, dataProvider = "getDataFor_1")
	public void testVerifyEndToEndBooking_01(Hashtable<String, String> data)
			throws InterruptedException {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		System.out.println("MethodName: " + methodName);

		APPLICATION_LOGS.info(">>>Starting execution of:'" + methodName + "'<<<");
		util.checkRunModeAndSkipTestCase(methodName, apage.admin_xls, data);
		
		//Access One World Home URL
		//common.deleteAllCookiesExample(driver);
		driver.get(conf.getValue("OW_Prod_URL"));
		
	
		//Authenticate accessing next pages
		//apage.authenticateToLoingOW(driver, conf.getValue("auth_password"));
		//Thread.sleep(10000);
		
		//Verify all the traveller options page wrappers
		apage.verifyTravellerOptionsPageWrappers(driver);
		
		//creating new itinerary and start the itinerary
		apage.cretaeNewItinerary(driver,data);
		
		//build the itinerary
		apage.buildItinerary(driver);
		//apage.captureScreenshotOnSuccess(driver,"buildItinerary");
		Thread.sleep(3000);
		
		//apage.preferedAirline(driver, data);
		//Select the dates
		apage.selectDate1(driver);
		apage.selectionDate1(driver, data);
		Thread.sleep(3000);
		apage.selectDate2(driver);
		apage.selectionDate2(driver, data);
		Thread.sleep(3000);
		apage.selectDate3(driver);
		apage.selectionDate3(driver, data);
		Thread.sleep(3000);
		apage.selectDate4(driver);
		apage.selectionDate4(driver, data);
		Thread.sleep(3000);
		apage.selectDate5(driver);
		apage.selectionDate5(driver, data);
		Thread.sleep(3000);
		
		//price my itinerary
		apage.pricemyItinerary(driver);
		//apage.captureScreenshotOnSuccess(driver,"pricemyItinerary");
		apage.captureScreenshotOnSuccess(driver,"pricemyItinerary");
		}

}// End of Class
