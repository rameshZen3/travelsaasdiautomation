package com.cwtdigital.qa.listener;
import org.bouncycastle.util.test.TestResult;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class EventListener implements WebDriverEventListener{
	final static Logger logback = LoggerFactory.getLogger(EventListener.class);
	private By lastFindBy;
	private WebElement lastElement;
	private String originalValue;

	 public void beforeNavigateTo(String url, WebDriver driver) {
		 logback.info("Before Navigating To : " + url + ", my url was: "+ driver.getCurrentUrl());
	 }

	  // Prints the current URL after Navigating to specific URL "get("http://www.google.com");"
	
	 public void afterNavigateTo(String url, WebDriver driver) {
		 logback.info("After Navigating To: " + url + ", my url is: "
	    + driver.getCurrentUrl());
	 }

	  // Prints the URL before Navigating back "navigate().back()"
	 
	 public void beforeNavigateBack(WebDriver driver) {
		 logback.info("Before Navigating Back. I was at "
	    + driver.getCurrentUrl());
	 }

	  // Prints the current URL after Navigating back "navigate().back()"
	
	 public void afterNavigateBack(WebDriver driver) {
		 logback.info("After Navigating Back. I'm at "
	    + driver.getCurrentUrl());
	 }

	  // Prints the URL before Navigating forward "navigate().forward()"
	 
	 public void beforeNavigateForward(WebDriver driver) {
		 logback.info("Before Navigating Forward. I was at "
	    + driver.getCurrentUrl());
	 }

	  // Prints the current URL after Navigating forward "navigate().forward()"
	 
	 public void afterNavigateForward(WebDriver driver) {
		 logback.info("After Navigating Forward. I'm at "
	    + driver.getCurrentUrl());
	 }


	/*
	 * ON EXCEPTION | SCREENSHOT, THROWING ERROR
	 */
	 // Takes screenshot on any Exception thrown during test execution
	
	 public void onException(Throwable throwable, WebDriver webdriver) {
		 
		 //logback.error("Exception occured in method "+webdriver.getClass().getMethods()+" : Exception is : "+throwable);
	 }


	/*
	 * FINDING ELEMENTS | FINDELEMENT() & FINDELEMENTS()
	 */
	 // Called before finding Element(s)
	
	 public void beforeFindBy(By by, WebElement element, WebDriver driver) {
	  lastFindBy = by;
	  logback.info("Trying to find: '" + lastFindBy + "'.");
	  logback.info("Trying to find: " + by.toString()); // This is optional and an alternate way
	 }

	 // Called after finding Element(s)
	
	 public void afterFindBy(By by, WebElement element, WebDriver driver) {
	  lastFindBy = by;
	  logback.info("Found: '" + lastFindBy + "'.");
	  logback.info("Found: " + by.toString() + "'."); // This is optional and an alternate way
	 }


	/*
	 * CLICK | CLICK()
	 */
	 // Called before clicking an Element
	 
	 public void beforeClickOn(WebElement element, WebDriver driver) {
		 logback.info("Trying to click: '" + element + "'");
	  // Highlight Elements before clicking
	  for (int i = 0; i < 1; i++) {
	   JavascriptExecutor js = (JavascriptExecutor) driver;
	   js.executeScript(
	     "arguments[0].setAttribute('style', arguments[1]);",
	     element, "color: black; border: 3px solid black;");
	  }
	 }

	 // Called after clicking an Element
	
	 public void afterClickOn(WebElement element, WebDriver driver) {
		 logback.info("Clicked Element with: '" + element + "'");
	 }


	/*
	 * CHANGING VALUES | CLEAR() & SENDKEYS()
	 */
	 // Before Changing values
	 
	 public void beforeChangeValueOf(WebElement element, WebDriver driver) {
	  lastElement = element;
	  originalValue = element.getText();

	   // What if the element is not visible anymore?
	  if (originalValue.isEmpty()) {
	   originalValue = element.getAttribute("value");
	  }
	 }

	  // After Changing values
	 
	 public void afterChangeValueOf(WebElement element, WebDriver driver) {
		 lastElement = element;
		 String changedValue = "";
		 try {
			 changedValue = element.getText();
		 } 
		 catch (StaleElementReferenceException e) {
			 logback.info("Could not log change of element, because of a stale" + " element reference exception.");
			 	return;
		 }
		 // What if the element is not visible anymore?
		 if (changedValue.isEmpty()) {
			 changedValue = element.getAttribute("value");
		 }

		 logback.info("Changing value in element found " + lastElement+ " from '" + originalValue + "' to '" + changedValue + "'");
	 }


	/*
	 * SCRIPT - this section will be modified ASAP
	 */
	 // Called before RemoteWebDriver.executeScript(java.lang.String, java.lang.Object[])
	 
	 public void beforeScript(String script, WebDriver driver) {
	 // TODO Auto-generated method stub
	 }

	  // Called before RemoteWebDriver.executeScript(java.lang.String, java.lang.Object[])
	  
	  public void afterScript(String script, WebDriver driver) {
	 // TODO Auto-generated method stub
	  }

	public void afterNavigateRefresh(WebDriver arg0) {
		// TODO Auto-generated method stub
		
	}

	public void beforeNavigateRefresh(WebDriver arg0) {
		// TODO Auto-generated method stub
		
	}

}
