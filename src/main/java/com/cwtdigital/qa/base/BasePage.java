package com.cwtdigital.qa.base;

import com.cwtdigital.qa.dataprovider.Config;
import com.cwtdigital.qa.dataprovider.Xls_Reader;
import com.cwtdigital.qa.util.CommonMethod;

public class BasePage {
		
	public static CommonMethod common = new CommonMethod(System.getProperty("user.dir")+ "\\locator\\objectRepository.properties");
	public  static Xls_Reader admin_xls = new Xls_Reader(System.getProperty("user.dir")+ "\\XLS\\testDataSheet.xls");
	public static Config validations = new Config(System.getProperty("user.dir")+"\\validations\\applicationValidations.properties");
	public static Config conf = new Config(System.getProperty("user.dir")+"\\config\\config.properties");
}
