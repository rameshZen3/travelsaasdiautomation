package com.cwtdigital.qa.driver;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import com.cwtdigital.qa.dataprovider.Config;

public class BrowserProvider implements IDriverProvider{
	String nameBrowser;
	//public static String browserName;
	Config conf=new Config(System.getProperty("user.dir")+"\\config\\config.properties");
	WebDriver driver;
	
	public WebDriver getDriver() {
			// TODO Auto-generated method stub
			if (driver==null){
				String browser=conf.getValue("driver.browsername");
				return getBrowser(browser);
		}
		return driver;
	}

	public WebDriver getDriver(String browserName) {
		// TODO Auto-generated method stub
		return getBrowser(browserName);
	}
	
	public WebDriver getBrowser(String browser){
		if(driver==null){
			if(browser.equalsIgnoreCase("firefox")){
				driver=new FirefoxDriver();
				return driver;
			}
			else if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", conf.getValue("chrome.path"));
				return new ChromeDriver();
			}
			else if(browser.equalsIgnoreCase("ie")){
				System.setProperty("webdriver.ie.driver", conf.getValue("ie.path"));
				return new InternetExplorerDriver();
			}
			else if(browser.equalsIgnoreCase("safari")){
				return new SafariDriver();
			}
			/*else if(browser.equalsIgnoreCase("html")){
				return new HtmlUnitDriver();
			}*/
			else if(browser.equalsIgnoreCase("phantom")){
				Proxy proxy=new Proxy();
				DesiredCapabilities dCaps = new DesiredCapabilities();
				dCaps.setCapability("applicationCacheEnabled", true);
				dCaps.setJavascriptEnabled(true);
				dCaps.setCapability("takesScreenshot", true);
				dCaps.setCapability(CapabilityType.PROXY, proxy);
				if(conf.getValue("platform").equalsIgnoreCase("linux")){
					dCaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/local/bin/phantomjs");
				}
				else{
					dCaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "C:/phantomjs.exe");
				}
				dCaps.setCapability("handlesAlerts", true);
				dCaps.setCapability("cssSelectorsEnabled", true);
				dCaps.setCapability("webdriver_accept_untrusted_certs", true);
				dCaps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] { "--web-security=false", "--ssl-protocol=any",
		               "--ignore-ssl-errors=true", "--webdriver-loglevel=DEBUG", "--local-to-remote-url-access=yes", "--disk-cache=false" });
				driver = new PhantomJSDriver(dCaps);
			}
		}
		return driver;
	}

	public String getBrowserName() {
		// TODO Auto-generated method stub
		return nameBrowser;
	}


}
